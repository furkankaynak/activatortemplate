import akka.actor.{Props, ActorSystem}
import server.TcpServer
import utils.Conf

object Main extends App {
  val system = ActorSystem("server")
  val service = system.actorOf(TcpServer.props(), "ServerActor")
}
