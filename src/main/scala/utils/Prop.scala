package utils

import java.net.InetSocketAddress
import akka.actor.{Props, ActorRef}

trait Prop {
  def props(connection: ActorRef, remote: InetSocketAddress): Props
}