package utils

import akka.actor.Actor.Receive
import akka.actor.Actor

trait Service extends Actor {
  def receive: Receive
}