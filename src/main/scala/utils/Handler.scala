package utils

import akka.actor.Actor.Receive
import akka.actor.Actor

trait Handler extends Actor {
  def receive: Receive
}
